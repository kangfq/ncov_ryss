<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Facades\Auth;
use WhichBrowser;

trait UserLog
{
    public function log()
    {
        //用户记录
        $result = new WhichBrowser\Parser(getallheaders());
        $os_name = $result->os->name;
        $os_version = $result->os->version->toString();
        $browser = $result->browser->name;
        $type = $result->device->type;
        $manufacturer = $result->device->manufacturer;
        $model = $result->device->model;

        $data['ip'] = request()->getClientIp();
        $data['device'] = $type.'-'.$manufacturer.'-'.$model;
        $data['browser'] = $browser;
        $data['os'] = $os_name.'-'.$os_version;
        $data['user_id'] = Auth::id();

        \App\UserLog::create($data);
    }
}
