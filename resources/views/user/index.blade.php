@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.index') }}">后台管理</a> / 用户管理 <span 　class="badge badge-pill badge-warning" style="float:right;">总用户数{{$count}}人</span></div>
                    <div class="card-header">
                        <form action="" method="get">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="name">用户名</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                           value="{{ Request::input('name') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="email">电话号码</label>
                                    <input type="text" class="form-control" id="email" name="email"
                                           value="{{ Request::input('email') }}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">筛选</button>
                            <a href="{{ route('admin.user.index') }}"><button type="button" class="btn btn-primary btn-sm">重置</button></a>
                        </form>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">name</th>
                                <th scope="col">tel</th>
                                <th scope="col">收货信息</th>
                                <th scope="col">管理权限</th>
                                <th scope="col">注册时间</th>
                                <th scope="col">管理</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td scope="row">{{ $user->id}}</td>
                                    <td scope="row">{{ $user->name}}</td>
                                    <td scope="row">{{ $user->email}}</td>
                                    <td scope="row">@if($user->address->count()==0) <span class="badge badge-pill badge-danger">否</span> @else <span class="badge badge-pill badge-success">是</span> @endif</td>
                                    <td scope="row">{{ $user->permissions}}</td>
                                    <td scope="row">{{ $user->created_at}}</td>
                                    <td scope="row"><a href="{{ route('admin.user.edit',$user->id) }}">编辑</a>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
